package com.synerzip.payroll.client.data;
/**
 * @author hussain.pithawala
 */
import com.smartgwt.client.data.DataSource;  
import com.smartgwt.client.data.fields.*;  
import com.smartgwt.client.types.DSDataFormat;

public class PaySlipDS extends DataSource{
	private static PaySlipDS instance = null;
	
	public static PaySlipDS getInstance(){
		if(instance == null){
			instance = new PaySlipDS("paySlipDS");
		}
		return instance;
	}
	
	private PaySlipDS(String id){
        this.setDataFormat(DSDataFormat.JSON);

        DataSourceIntegerField employeeIdField = new DataSourceIntegerField("employeeId");
        employeeIdField.setHidden(true);
        employeeIdField.setRequired(true);
        
        DataSourceTextField particularsField = new DataSourceTextField("particulars", "Particulars");  
        particularsField.setRequired(true);  
  
        DataSourceFloatField basicField = new DataSourceFloatField("basic","Basic");
        basicField.setRequired(true);
  
        DataSourceFloatField taxField = new DataSourceFloatField("taxDeducted","Tax Deducted");
        taxField.setRequired(true);
        
        DataSourceFloatField netField = new DataSourceFloatField("net","Net");
        netField.setRequired(true);
        
        setFields(particularsField,basicField,taxField,netField);  
  
        setDataURL("rest/payslip/allByEmpId/100");  
        setClientOnly(true);
	}
}
