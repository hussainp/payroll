package com.synerzip.payroll.client.data;

import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.*;

public class TaxDeclXMLDS extends DataSource{
	private static TaxDeclXMLDS instance = null;
	
	public static TaxDeclXMLDS getInstance(){
		if(instance == null){
			instance = new TaxDeclXMLDS("taxDeclDS");
		}
		return instance;
	}
	
	private TaxDeclXMLDS(String id){
		setID(id);  	
        setRecordXPath("/List/TaxDecl");
        
        DataSourceIntegerField pkField = new DataSourceIntegerField("pk");  
        pkField.setHidden(true);  
        pkField.setPrimaryKey(true);  
  
        DataSourceTextField taxDeclId = new DataSourceTextField("taxDeclId", "TaxDeclId");  
        taxDeclId.setRequired(true);  
 
        DataSourceTextField typeField = new DataSourceTextField("typeDecl","Type");
        typeField.setRequired(true);
        
        DataSourceTextField particulars = new DataSourceTextField("particulars", "Particulars");  
        particulars.setRequired(true);  
  
        DataSourceBooleanField paid = new DataSourceBooleanField("paid", "Paid");  
        paid.setRequired(true);
        
        DataSourceDateField investmentDate = new DataSourceDateField("investmentDate", "Date");
        investmentDate.setRequired(true);
  
        setFields(pkField, taxDeclId, typeField, particulars, paid, investmentDate);  
  
        setDataURL("ds/taxdecl.data.xml");  
        setClientOnly(true);
	}
}
