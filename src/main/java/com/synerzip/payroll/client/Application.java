package com.synerzip.payroll.client;

import com.google.gwt.core.client.EntryPoint;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Side;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tab.events.TabDeselectedEvent;
import com.smartgwt.client.widgets.tab.events.TabDeselectedHandler;
import com.smartgwt.client.widgets.tab.events.TabSelectedEvent;
import com.smartgwt.client.widgets.tab.events.TabSelectedHandler;


/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Application implements EntryPoint {

  /**
   * This is the entry point method.
   */
  public void onModuleLoad() {
	  final TabSet topTabSet = new TabSet();  
      topTabSet.setTabBarPosition(Side.TOP);
      topTabSet.setTop(100);
      topTabSet.setLeft(200);
      topTabSet.setWidth(800);  
      topTabSet.setHeight(600);  
      
      Tab tTab1 = new Tab("Profile");

      Tab tTab2 = new Tab("Tax Declaration");
      
      /**
       * Display tax declaration grid on tab selection
       */
      tTab2.addTabSelectedHandler(new TabSelectedHandler() {
		
		public void onTabSelected(TabSelectedEvent event) {
			ListGrid taxDeclGrid = TaxDeclContainer.getInstance().getTaxDeclContainer();
			taxDeclGrid.draw();
			event.getTab().setAttribute("taxGrid", taxDeclGrid);
		}
      });
      
      /**
       * Hide tax declaration grid on tab de-selection
       */
      tTab2.addTabDeselectedHandler(new TabDeselectedHandler() {
		public void onTabDeselected(TabDeselectedEvent event) {
			ListGrid taxDeclGrid = (ListGrid)event.getTab().getAttributeAsObject("taxGrid");
			taxDeclGrid.hide();
		}
      });
      
      Tab tTab3 = new Tab("PaySlip");
      /**
       * Display salary grid on tab selection
       */
      tTab3.addTabSelectedHandler(new TabSelectedHandler() {
		public void onTabSelected(TabSelectedEvent event) {
			VLayout printContainer = PaySlipContainer.getPaySlipContainer();
			event.getTab().setAttribute("printContainer", printContainer);
	        printContainer.draw();
		}
      });
      
      /**
       * Hide salary grid on tab deselection
       */
      tTab3.addTabDeselectedHandler(new TabDeselectedHandler() {
		public void onTabDeselected(TabDeselectedEvent event) {
			VLayout printContainer = (VLayout)event.getTab().getAttributeAsObject("printContainer");
			
			if(printContainer != null)
				printContainer.hide();
		}
      });
      
      topTabSet.addTab(tTab1); 
      topTabSet.addTab(tTab2);
      topTabSet.addTab(tTab3);  

      HLayout buttons = new HLayout();  
      buttons.setMembersMargin(15);  

      VLayout vLayout = new VLayout();
      vLayout.setMembersMargin(15);  
      vLayout.addMember(topTabSet);  
      vLayout.addMember(buttons);
      vLayout.setLeft(100);
      vLayout.setTop(200);
      vLayout.setHeight("auto");  
      
      // set the alignment to center
      vLayout.setAlign(Alignment.CENTER);
      vLayout.draw();
  }
}
