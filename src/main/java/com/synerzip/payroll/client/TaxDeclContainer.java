package com.synerzip.payroll.client;

import com.smartgwt.client.data.RecordList;
import com.smartgwt.client.data.ResultSet;
import com.smartgwt.client.data.events.DataChangedEvent;
import com.smartgwt.client.data.events.DataChangedHandler;
import com.smartgwt.client.types.ListGridComponent;
import com.smartgwt.client.types.ListGridEditEvent;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;
import com.synerzip.payroll.client.data.TaxDeclXMLDS;

public class TaxDeclContainer {
	
	private static TaxDeclContainer instance = null;
	
	public static TaxDeclContainer getInstance(){
		if(instance == null)
			instance = new TaxDeclContainer();
		
		return instance;
	}
	
	private TaxDeclContainer() {
	}
	
	public ListGrid getTaxDeclContainer(){
		return prepareTaxDeclContainer();
	}
	
	private ListGrid taxDeclGrid;
	private Label totalsLabel;
	
	private ListGrid prepareTaxDeclContainer(){
		taxDeclGrid = new ListGrid();
		
        ToolStrip gridEditControls = new ToolStrip();  
        gridEditControls.setWidth100();  
        gridEditControls.setHeight(24);  
          
        totalsLabel = new Label();  
        totalsLabel.setPadding(5);  
          
        LayoutSpacer spacer = new LayoutSpacer();  
        spacer.setWidth("*");  
          
        ToolStripButton editButton = new ToolStripButton();  
        editButton.setIcon("[SKIN]/actions/edit.png");  
        editButton.setPrompt("Edit selected record");  

        editButton.addClickHandler(new ClickHandler() {  
            public void onClick(ClickEvent event) {  
                ListGridRecord record = taxDeclGrid.getSelectedRecord();  
                if (record == null) return;  
                taxDeclGrid.startEditing(taxDeclGrid.getDataAsRecordList().indexOf(record), 0, false);  
            }  
        });  
          
        ToolStripButton removeButton = new ToolStripButton();  
        removeButton.setIcon("[SKIN]/actions/remove.png");  
        removeButton.setPrompt("Remove selected record");  

        removeButton.addClickHandler(new ClickHandler() {  
            public void onClick(ClickEvent event) {  
                taxDeclGrid.removeSelectedData();  
            }  
        });  
          
        gridEditControls.setMembers(totalsLabel, spacer, editButton, removeButton);  
          
        ListGridField taxDeclId = new ListGridField("TaxDeclId");  
        ListGridField type = new ListGridField("Type");  
        ListGridField particulars = new ListGridField("Particulars");  
        ListGridField paid = new ListGridField("Paid");
        ListGridField date = new ListGridField("Date");
  
          
        taxDeclGrid.setFields(taxDeclId, type, particulars, paid , date);
        taxDeclGrid.setTop(225);
        taxDeclGrid.setLeft(125);
        taxDeclGrid.setWidth(500);  
        taxDeclGrid.setHeight("50%");  
        taxDeclGrid.setDataSource(TaxDeclXMLDS.getInstance());  
        taxDeclGrid.setAutoFetchData(true);  
        taxDeclGrid.setShowFilterEditor(true);  
        taxDeclGrid.setCanEdit(true);  
        taxDeclGrid.setEditEvent(ListGridEditEvent.NONE);  
          
        ResultSet dataProperties = new ResultSet();  
        dataProperties.addDataChangedHandler(new DataChangedHandler() {  
              
            public void onDataChanged(DataChangedEvent event) {  
                RecordList data = taxDeclGrid.getDataAsRecordList();  
                  
                if (data != null && data instanceof ResultSet && ((ResultSet)data).lengthIsKnown() && data.getLength() > 0) {  
                    totalsLabel.setContents(data.getLength() + " Records");  
                } else {  
                    totalsLabel.setContents(" ");  
                }  
            }  
        });  
        taxDeclGrid.setDataProperties(dataProperties);  
        
        taxDeclGrid.setGridComponents(new Object[] {  
                ListGridComponent.HEADER,   
                ListGridComponent.FILTER_EDITOR,   
                ListGridComponent.BODY,   
                gridEditControls  
        });  
        
        return taxDeclGrid;
	}
}
