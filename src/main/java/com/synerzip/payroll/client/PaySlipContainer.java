package com.synerzip.payroll.client;
/**
 * @author hussain.pithawala
 */
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.VisibilityMode;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.SectionStack;
import com.smartgwt.client.widgets.layout.SectionStackSection;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.viewer.DetailViewer;
import com.synerzip.payroll.client.data.PaySlipDS;


public class PaySlipContainer{
	
	public static VLayout getPaySlipContainer(){
			return preparePaySlipContainer();
	}
	
	private static VLayout preparePaySlipContainer(){
		PaySlipDS paySlipDS = PaySlipDS.getInstance();
		
		SectionStack printStack = new SectionStack();  
        printStack.setVisibilityMode(VisibilityMode.MULTIPLE);  
        printStack.setWidth(400);  
        printStack.setHeight(455);
        
        final DetailViewer printViewer = new DetailViewer();  
        printViewer.setDataSource(paySlipDS);  
        printViewer.setWidth100();  
        printViewer.setMargin(15);  
        printViewer.setEmptyMessage("Select a month to view its payslip.");
        
        final ListGrid printGrid = new ListGrid();  
        printGrid.setHeight(150);  
  
        printGrid.setDataSource(paySlipDS);  
  
        ListGridField particulars = new ListGridField("particulars", "Particulars", 150);  
          
        printGrid.setFields(particulars);  
  
        printGrid.addRecordClickHandler(new RecordClickHandler() {  
            public void onRecordClick(RecordClickEvent event) {  
                printViewer.setData(new Record[]{event.getRecord()});  
            }  
        });
        
        SectionStackSection monthsSection = new SectionStackSection("Month and Year");  
        monthsSection.setExpanded(true);  
        monthsSection.addItem(printGrid);  
        printStack.addSection(monthsSection);
        
        /**
         * Salary Details for the selected month
         */
        SectionStackSection detailsSection = new SectionStackSection("Salary Details");  
        detailsSection.setExpanded(true);  
        detailsSection.addItem(printViewer);  
        printStack.addSection(detailsSection);
        
        final VLayout printContainer = new VLayout(10);  
        
        HLayout printButtonLayout = new HLayout(5);  

        printContainer.addMember(printStack);  
        printContainer.addMember(printButtonLayout);
        printContainer.setLeft(125);
        printContainer.setTop(225);
  
  
        // The filter is just to limit the number of records in the ListGrid - we don't want to print them all  
        printGrid.filterData(new Criteria("MonthName", "salary"), new DSCallback() {
            public void execute(DSResponse response, Object rawData, DSRequest request) {  
                printGrid.selectRecord(0);  
                printViewer.setData(new Record[]{printGrid.getSelectedRecord()});  
            }  
        });  
        
        return printContainer;
	}
}
