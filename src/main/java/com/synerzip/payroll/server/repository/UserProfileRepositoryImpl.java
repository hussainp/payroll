package com.synerzip.payroll.server.repository;

import org.springframework.stereotype.Repository;

import com.synerzip.payroll.server.model.UserProfile;

@Repository
public class UserProfileRepositoryImpl extends AbstractRepository<UserProfile> implements UserProfileRepository{

	@Override
	public void setCollecionName() {
		this.collectionName = "userProfile";
	}

	@Override
	public void setCollectionClass() {
		this.collectionClass = UserProfile.class; 
	}
	
}
