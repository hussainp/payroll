package com.synerzip.payroll.server.repository;

import java.util.List;

public interface BaseRepository<T> {
	public List<T> findAll();
	public void remove(T t);
	public void save(T t);
	public T update(T t);
}
