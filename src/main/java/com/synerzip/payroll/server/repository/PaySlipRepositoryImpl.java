package com.synerzip.payroll.server.repository;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.data.document.mongodb.query.Criteria;
import org.springframework.data.document.mongodb.query.Query;
import org.springframework.stereotype.Repository;

import com.synerzip.payroll.server.model.PaySlip;

@Repository
public class PaySlipRepositoryImpl extends AbstractRepository<PaySlip> implements PaySlipRepository{

	@Override
	@PostConstruct
	public void setCollecionName() {
		this.collectionName = "paySlip";
	}

	@Override
	@PostConstruct
	public void setCollectionClass() {
		this.collectionClass = PaySlip.class; 
	}

	@Override
	public List<PaySlip> findAll(Long employeeId) {
		return (List<PaySlip>) this.getMongoOperations().find(this.collectionName,new Query(Criteria.where("employeeId").is(employeeId)),this.collectionClass);
	}

}
