package com.synerzip.payroll.server.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.document.mongodb.MongoOperations;

public abstract class AbstractRepository<T>{
	private MongoOperations mongoOperations;
	
	protected String collectionName;
	
	protected Class<?> collectionClass;
	
	public abstract void setCollecionName();
	
	public abstract void setCollectionClass();
	
	public MongoOperations getMongoOperations() {
		return mongoOperations;
	}

	@Autowired
	public void setMongoOperations(MongoOperations mongoOperations) {
		this.mongoOperations = mongoOperations;
	}

	public List<T> findAll() {
		return (List<T>)mongoOperations.getCollection(collectionClass);
	}

	public void remove(T t) {
		mongoOperations.remove(t);
	}

	public void save(T t) {
		mongoOperations.save(t);
	}

	public T update(T t) {
		return null;
	}

}
