package com.synerzip.payroll.server.repository;

import com.synerzip.payroll.server.model.UserProfile;

public interface UserProfileRepository extends BaseRepository<UserProfile> {

}
