package com.synerzip.payroll.server.repository;

import java.util.List;

import com.synerzip.payroll.server.model.PaySlip;

public interface PaySlipRepository extends BaseRepository<PaySlip> {
	List<PaySlip> findAll(Long employeeId);
}
