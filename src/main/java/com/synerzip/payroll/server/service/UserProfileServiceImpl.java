package com.synerzip.payroll.server.service;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.synerzip.payroll.server.model.UserProfile;
import com.synerzip.payroll.server.repository.UserProfileRepository;

@Service
public class UserProfileServiceImpl extends AbstractService<UserProfile> implements UserProfileService{
	
	@Autowired
	private UserProfileRepository userProfileRepository;
	
	public UserProfileRepository getUserProfileRepository() {
		return userProfileRepository;
	}
	
	public void setUserProfileRepository(
			UserProfileRepository userProfileRepository) {
		this.userProfileRepository = userProfileRepository;
	}

	@Override
	@PostConstruct
	public void setBaseRepository() {
		this.baseRepository = this.userProfileRepository;
	}

}
