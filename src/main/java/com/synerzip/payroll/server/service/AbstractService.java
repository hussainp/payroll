package com.synerzip.payroll.server.service;

import java.util.List;

import com.synerzip.payroll.server.repository.BaseRepository;

public abstract class AbstractService<T> {
	protected BaseRepository<T> baseRepository;
	
	public abstract void setBaseRepository();
	
	public List<T> findAll(){
		return baseRepository.findAll();
	}

	public void remove(T t) {
		baseRepository.remove(t);
	}

	public void save(T t) {
		baseRepository.save(t);
	}

	public T update(T t) {
		return baseRepository.update(t);
	}
}
