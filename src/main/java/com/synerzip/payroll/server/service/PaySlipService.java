package com.synerzip.payroll.server.service;

import java.util.List;

import com.synerzip.payroll.server.model.PaySlip;

public interface PaySlipService extends BaseService<PaySlip> {
	List<PaySlip> findAll(Long employeeId);
}
