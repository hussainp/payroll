package com.synerzip.payroll.server.service;

import com.synerzip.payroll.server.model.UserProfile;

public interface UserProfileService extends BaseService<UserProfile> {

}
