package com.synerzip.payroll.server.service;

import java.util.List;

import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.synerzip.payroll.server.model.PaySlip;
import com.synerzip.payroll.server.repository.PaySlipRepository;
import com.synerzip.payroll.server.repository.PaySlipRepositoryImpl;

@Service
public class PaySlipServiceImpl extends AbstractService<PaySlip> implements PaySlipService{
	@Autowired
	private PaySlipRepository paySlipRepository;
	
	public PaySlipRepository getPaySlipRepository() {
		return paySlipRepository;
	}

	public void setPaySlipRepository(PaySlipRepositoryImpl paySlipRepository) {
		this.paySlipRepository = paySlipRepository;
	}

	@Override
	@PostConstruct
	public void setBaseRepository() {
		this.baseRepository = paySlipRepository;
	}

	@Override
	public List<PaySlip> findAll(Long employeeId) {
		return paySlipRepository.findAll(employeeId);
	}

}
