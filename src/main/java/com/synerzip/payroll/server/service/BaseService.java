package com.synerzip.payroll.server.service;

import java.util.List;

public interface BaseService<T> {
	public List<T> findAll();
	public void remove(T t);
	public void save(T t);
	public T update(T t);
}
