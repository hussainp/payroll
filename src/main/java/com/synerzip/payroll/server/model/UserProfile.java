package com.synerzip.payroll.server.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.document.mongodb.mapping.Document;

/**
 * 
 * @author hussain.pithawala
 *
 */
@Document
public class UserProfile extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Override
	public Long fetchId() {
		return userProfileId;
	}
	
	@Id
	private Long userProfileId;
	private Long employeeId;
	private String firstName;
	private String lastName;
	private Integer age;
	private String department;

	public Long getUserProfileId() {
		return userProfileId;
	}
	public void setUserProfileId(Long userProfileId) {
		this.userProfileId = userProfileId;
	}
	public Long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
}
