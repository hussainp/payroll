package com.synerzip.payroll.server.model;
/**
 * 
 * @author hussain.pithawala
 *
 */
public class PaySlip extends BaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long empId;
	private Float basic;
	private Float taxDeducted;
	private Float net;
	private String particulars;
	
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
	public Float getBasic() {
		return basic;
	}
	public void setBasic(Float basic) {
		this.basic = basic;
	}
	public Float getTaxDeducted() {
		return taxDeducted;
	}
	public void setTaxDeducted(Float taxDeducted) {
		this.taxDeducted = taxDeducted;
	}
	public Float getNet() {
		return net;
	}
	public void setNet(Float net) {
		this.net = net;
	}
	
	@Override
	public Long fetchId() {
		return getEmpId();
	}
	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}
	public String getParticulars() {
		return particulars;
	}
}
