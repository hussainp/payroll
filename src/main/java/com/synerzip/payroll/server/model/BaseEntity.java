package com.synerzip.payroll.server.model;
import java.io.Serializable;

@SuppressWarnings("serial")
public abstract class BaseEntity implements Serializable{
	public abstract Long fetchId();
}