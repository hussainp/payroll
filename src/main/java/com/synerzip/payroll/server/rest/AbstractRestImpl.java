package com.synerzip.payroll.server.rest;

import java.util.List;

import com.synerzip.payroll.server.model.BaseEntity;
import com.synerzip.payroll.server.service.BaseService;

public abstract class AbstractRestImpl<T extends BaseEntity> {
	protected BaseService<T> baseService;
	
	public T getById(Long id){
		return null;
	}

	public List<T> findAll(){
		return baseService.findAll();
	}

	public void save(T t){
		baseService.save(t);
	}

	public void remove(T t){
		baseService.remove(t);
	}

	public abstract void setBaseService();

	public T update(T t){
		return baseService.update(t);
	}
}
