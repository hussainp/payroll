package com.synerzip.payroll.server.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.synerzip.payroll.server.model.PaySlip;
import com.synerzip.payroll.server.service.PaySlipService;

@Service("PaySlipRest")
@Path("payslip/")
@Produces("application/json")
@Consumes("application/json")
public class PaySlipRestImpl extends AbstractRestImpl<PaySlip> implements PaySlipRest{
	private PaySlipService paySlipService;
	
	public PaySlipService getPaySlipService() {
		return paySlipService;
	}

	@Autowired
	public void setPaySlipService(PaySlipService paySlipService) {
		this.paySlipService = paySlipService;
	}

	@Override
	public void setBaseService() {
		this.baseService = this.paySlipService;
	}

	/**
	 * Fetch the payslip by an payslipId
	 */
	@Override
	@GET
	@Path("/bySlipId/{longParam}")
	public PaySlip getById(@PathParam("longParam") Long id){
		return null;
	}
	
	@GET
	@Path("/allByEmpId/{employeeId}")
	public List<PaySlip> findAll(@PathParam("employeeId")Long employeeId){
		return paySlipService.findAll(employeeId);
	}
}
