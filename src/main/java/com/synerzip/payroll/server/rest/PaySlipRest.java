package com.synerzip.payroll.server.rest;

import java.util.List;

import com.synerzip.payroll.server.model.PaySlip;


public interface PaySlipRest extends BaseRest<PaySlip>{
	List<PaySlip> findAll(Long employeeId);
}
