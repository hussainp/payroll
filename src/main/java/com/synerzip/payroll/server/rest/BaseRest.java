package com.synerzip.payroll.server.rest;

import java.util.List;

public interface BaseRest<T> {
	public List<T> findAll();

	public void save(T t);

	public void remove(T t);

	public abstract void setBaseService();

	public T update(T t);
}
